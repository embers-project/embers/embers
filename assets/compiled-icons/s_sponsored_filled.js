/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  's_sponsored_filled': {
    width: 16,
    height: 16,
    viewBox: '-50 51 411 411',
    data: '<path pid="0" d="M150.2 51h10.5c8.9 1.6 16.6 8 20.1 16.3 15.8 35.5 31.5 71.1 47.4 106.5h105.7c13 .1 24.7 10.1 27.1 22.8v9.5c-1.3 6.3-4.5 12.2-9.5 16.4-27.6 23.5-55 47.2-82.6 70.7 11.5 42.1 23 84.3 34.5 126.5 1.8 6.3 3.9 12.8 2.7 19.4-1.8 12.1-12.5 21.9-24.7 23h-4.2c-5.7-.6-11.3-2.7-15.8-6.3-35.2-27.5-70.6-54.8-105.8-82.3C120 401 84.5 428.6 49 456.2c-4.2 3.3-9.4 5-14.6 5.8h-4.2c-16.4-.7-29.2-17.9-24.9-33.7 12.2-45 24.5-90 36.8-135C16.7 271.5-8.6 249.7-34 228c-7.3-5.6-14.4-12.3-16-21.8v-9.4c2.2-12.9 14-23 27.1-23.1H82.8c16-35.7 31.8-71.5 47.7-107.2 3.6-7.8 11.1-13.9 19.7-15.5"/>'
  }
})
