const state = {
  unread_conversations: [],
  show_new_chat_modal: false,
  online_friends: {}
};

export default state;
