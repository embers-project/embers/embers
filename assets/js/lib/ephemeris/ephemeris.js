export default [{
  img: "/img/efemerides/arg_bandera.svg",
  desc: "9 de Julio, Día de la Independencia Argentina",
  day: 9,
  month: 7
},
{
  img: "/img/efemerides/arg_bandera.svg",
  desc: "25 de Mayo, 210 aniversario de la Revolución de Mayo",
  day: 25,
  month: 5
}]
