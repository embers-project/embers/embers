/**
 * @todo retrieve custom emotes from server
 */

export default [{
    name: "Smutley",
    short_names: ["smutley"],
    keywords: ["smutley"],
    imageUrl: "/img/emotes/smutley.png"
  },
  {
    name: "Paja",
    short_names: ["paja"],
    keywords: ["paja"],
    imageUrl: "/img/emotes/paja.png"
  },
  {
    name: "Grin",
    short_names: ["grin"],
    keywords: ["grin"],
    imageUrl: "/img/emotes/grin.svg"
  },
  {
    name: "Pepe",
    short_names: ["pepe"],
    keywords: ["pepe"],
    imageUrl: "/img/emotes/pepe.png"
  },
  {
    name: "Angry Sock",
    short_names: ["angrysock", "angry_sock"],
    keywords: ["angry_sock", "angrysock", "angry", "sock"],
    imageUrl: "/img/emotes/angrysock.png"
  },
  {
    name: "Shipit",
    short_names: ["shipit"],
    keywords: ["shipit"],
    imageUrl: "/img/emotes/shipit.png"
  },
  {
    name: "Pls",
    short_names: ["pls"],
    keywords: ["pls"],
    imageUrl: "/img/emotes/pls.png"
  },
  {
    name: "Sanguche",
    short_names: ["sanguche"],
    keywords: ["sanguche"],
    imageUrl: "/img/emotes/sanguche.png"
  },
  {
    name: "Horsello",
    short_names: ["horsello"],
    keywords: ["horsello"],
    imageUrl: "/img/emotes/horsello.png"
  },
  {
    name: "Omegalul",
    short_names: ["omegalul"],
    keywords: ["omegalul"],
    imageUrl: "/img/emotes/omegalul.png"
  },
  {
    name: "Dorgan",
    short_names: ["dorgan"],
    keywords: ["dorgan"],
    imageUrl: "/img/emotes/dorgan.png"
  },
  {
    name: "Tomwat",
    short_names: ["tomwat"],
    keywords: ["tomwat"],
    imageUrl: "/img/emotes/tomwat.png"
  },
  {
    name: "Kuru",
    short_names: ["kuru"],
    keywords: ["kuru"],
    imageUrl: "/img/emotes/kuru.png"
  },
  {
    name: "Bob",
    short_names: ["bob"],
    keywords: ["bob"],
    imageUrl: "/img/emotes/bob.png"
  },
  {
    name: "corona",
    short_names: ["corona"],
    keywords: ["corona"],
    imageUrl: "/img/emotes/corona.png"
  },
  {
    name: "Comfy",
    short_names: ["comfy"],
    keywords: ["comfy"],
    imageUrl: "/img/emotes/comfy.png"
  },
  {
    name: "Coffee Cup",
    short_names: ["w_coffeecup"],
    keywords: ["coffeecup", "coffee", "cup"],
    imageUrl: "/img/emotes/_white/coffeecup.svg"
  },
  {
    name: "Heart",
    short_names: ["w_heart"],
    keywords: ["heart"],
    imageUrl: "/img/emotes/_white/heart.svg"
  }
];
