export const baseUrl = window.appData.baseUrl;
export const csrfToken = window.appData.csrfToken;
export const recaptchaPublicKey = window.appData.recaptcha;
export const skipRecaptcha = window.appData.skip_recaptcha;